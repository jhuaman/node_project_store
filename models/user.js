var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/store',{ useNewUrlParser: true,useUnifiedTopology: true  });

var userSchemaJSON = {    
    username: String,
    password: String	
};

var user_schema = new Schema(userSchemaJSON,{ collection: 'User' });
var User = mongoose.model("User",user_schema);

module.exports.User = User;