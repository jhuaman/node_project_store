var express = require("express");
var bodyParser= require('body-parser');

var User = require("./models/user").User;

var app = express();

app.use('/public',express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set("view engine","jade");

//pagina principal
app.get("/",function(req,res){
	res.render("index");
});

//pagina de login
app.get("/login",function(req,res){
	User.find(function(err,doc){
		console.log(doc);
		res.render("login");
	});
});

//grabar usuario
app.post("/save",function(req,res)
	{
    if (!req.body) return res.sendStatus(400);
		
		console.log(req.body);
		console.log("Contraseña: " + req.body.password);
		console.log("Username: " + req.body.username);

		var user = new User({username: req.body.username, password: req.body.password});
		
		user.save(function(err)
		{
			if (err) return handleError(err);
			res.send("Guardamos tus datos");
		});
});

app.listen(8001);